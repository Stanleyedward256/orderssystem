﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Repository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    /// <summary>
    /// Controller that handles getting the details of the items on a specific order
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrderItemController : ControllerBase
    {
        /// <summary>
        /// Local reference to our injected data repository.
        /// </summary>
        IDataRepository DataRepository;

        /// <summary>
        /// Constructor that sets our data repository.
        /// </summary>
        /// <param name="dataRepository">Our injected data repository</param>
        public OrderItemController(IDataRepository dataRepository)
        {
            DataRepository = dataRepository;
        }

        /// <summary>
        /// List of order items for a specifed order ref
        /// </summary>
        /// <param name="orderRef">The order's reference.</param>
        /// <returns>200 with a list of items, or 204 if no items found</returns>
        [HttpGet("{orderRef}")]
        public IEnumerable<OrderItem> Get(string orderRef)
        {
            return DataRepository.Orders.GetOrderItems(orderRef);   
        }
    }
}
