﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Repository;

namespace API.Controllers
{
    /// <summary>
    /// Controller that handles the getting or order data.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        /// <summary>
        /// Local reference to our injected data repository.
        /// </summary>
        private readonly IDataRepository DataRepository;

        /// <summary>
        /// Constructor that sets our data repository.
        /// </summary>
        /// <param name="dataRepository">Our injected data repository</param>
        public OrderController(IDataRepository dataRepository)
        {
            DataRepository = dataRepository;
        }

        /// <summary>
        /// Reads all the orders from the system
        /// </summary>
        /// <returns>200 and a collection of Orders, or 204 if no orders in the system</returns>
        [HttpGet]
        public IEnumerable<Order> Get()
        {
            return (DataRepository.Orders.GetOrders());
        }
    }
}
