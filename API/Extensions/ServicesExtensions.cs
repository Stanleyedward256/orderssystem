﻿using Microsoft.Extensions.DependencyInjection;
using Repository;

namespace API.Extensions
{
    /// <summary>
    /// Extension methods relating to service registering.
    /// </summary>
    public static class ServicesExtensions
    {
        /// <summary>
        /// Sets up our data repository as an injected parameter that will then be accessible
        /// on our controllers.
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureRepository(this IServiceCollection services)
        {
            services.AddScoped<IDataRepository, DataRepository>();
        }

    }
}
