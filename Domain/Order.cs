﻿namespace Domain
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using System.Linq;
    using Extensions;

    /// <summary>
    /// Represents a single order in the system
    /// </summary>
    [JsonObject]
    [Serializable]
    public class Order
    {
        public Order()
        {
            Items = new List<OrderItem>();
        }

        /// <summary>
        /// Reference number for the order
        /// </summary>
        public string Ref { get; set; }

        /// <summary>
        /// Items on the order
        /// </summary>
        public IList<OrderItem> Items { get; set; }

        /// <summary>
        /// The total sum of all the items on this order represented as a £ string.
        /// </summary>
        public string FormattedOrderTotal {
            get
            {
                decimal total = Items.Sum(i => (i.Cost * i.Quantity));

                return total.ToPoundString();
            }
        }
    }
}
