﻿namespace Domain
{
    using System;
    using Newtonsoft.Json;


    /// <summary>
    /// Represents an available product in the system.
    /// </summary>
    [JsonObject]
    [Serializable]
    public class Product
    {
        /// <summary>
        /// Reference code for the item.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// User friendly description of the product.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Cost of the product.
        /// </summary>
        public decimal Cost { get; set; }
    }
}
