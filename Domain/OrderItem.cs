﻿namespace Domain
{
    using System;
    using Extensions;
    using Newtonsoft.Json;

    /// <summary>
    /// Represents a single item on an order
    /// </summary>
    [JsonObject]
    [Serializable]
    public class OrderItem : Product
    {
        /// <summary>
        /// How many of the product constitutes this order item
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// The cost of the item times by the quantity of that item.
        /// </summary>
        public decimal TotalCost
        {
            get
            {
                return (Cost * Quantity);
            }
        }

        /// <summary>
        /// The base cost of the item as a string prefixed with £
        /// </summary>
        public string FormattedCost
        {
            get
            {
                return Cost.ToPoundString();
            }
        }

        /// <summary>
        /// The cost of the item multiplied by the quantity as a string
        /// </summary>
        public string FormattedTotalCost
        {
            get
            {
                return TotalCost.ToPoundString();
            }
        }
    }
}
