﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Extensions
{
    /// <summary>
    /// Extension method class for the decimal class.
    /// </summary>
    public static class DecimalExtensions
    {
        /// <summary>
        /// Extension to represent the decimal number as a £ currency string.
        /// </summary>
        /// <param name="amount">Decimal to represent as a £ and pennies currency string.</param>
        /// <returns>The value to 2 decimal places, prefixed with a £</returns>
        public static string ToPoundString(this decimal amount)
        {
            string result = "£" + amount.ToString("N2");

            return result;
        }
    }
}
