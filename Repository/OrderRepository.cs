﻿namespace Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using Domain;

    /// <summary>
    /// Implementation of the order repository.
    /// </summary>
    public class OrderRepository : IOrderRepository
    {
        #region Data store properties/constructors
        /// <summary>
        /// Local collection of orders
        /// </summary>
        /// <remarks>This is just a way of representing a more flexible data store for demo purposes.</remarks>
        private readonly List<Order> _orders;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <remarks>In this case this builds our list of orders.</remarks>
        public OrderRepository()
        {

            _orders = new List<Order>
            {
                new Order
                {
                    Ref = "ABC123",
                    Items = new List<OrderItem>
                    {
                        new OrderItem {Code = "HAT", Description = "Wool Hat with bobble on top", Cost = (decimal) 5.00, Quantity = 1},
                        new OrderItem {Code = "JUMPER", Description = "Knitted style jumper with large buttons", Cost = (decimal) 12.65, Quantity = 2},
                        new OrderItem {Code = "TROUSERS", Description = "Long Slim Fit Denim Jeans", Cost = (decimal) 35.25, Quantity = 3}
                    }
                },
                new Order
                {
                    Ref = "DEF456",
                    Items = new List<OrderItem>
                    {
                        new OrderItem {Code = "HAT", Description = "Wool Hat with bobble on top", Cost = (decimal) 5.00, Quantity = 1},
                        new OrderItem {Code = "JUMPER", Description = "Knitted style jumper with large buttons", Cost = (decimal) 12.65, Quantity = 2}
                    }
                },
                new Order
                {
                    Ref = "GHI456"
                },
            };
        }
        #endregion

        #region Order Detail Methods
        /// <summary>
        /// All the orders in the system
        /// </summary>
        /// <returns>A collection of all orders in the system</returns>
        public List<Order> GetOrders()
        {
            return _orders;
        }

        #endregion

        #region Order Item Detail Methods

        /// <summary>
        /// Obtain Order items for a specified order.
        /// </summary>
        /// <param name="orderRef">The reference of the order to load</param>
        /// <returns>A collection of order items for the specified order</returns>
        public List<OrderItem> GetOrderItems(string orderRef)
        {
            return (from o in _orders where o.Ref == orderRef select o.Items as List<OrderItem>).FirstOrDefault();
        }

        #endregion
    }
}
