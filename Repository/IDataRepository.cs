﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    /// <summary>
    /// Represents a type independent version of a data repository.
    /// </summary>
    public interface IDataRepository
    {
        /// <summary>
        /// Represents access to the orders repository
        /// </summary>
        IOrderRepository Orders { get; }
    }
}
