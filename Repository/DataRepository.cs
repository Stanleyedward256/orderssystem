﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    /// <summary>
    /// Wrapper around our various repositories.
    /// </summary>
    public class DataRepository : IDataRepository
    {
        /// <summary>
        /// local reference to orders controller
        /// </summary>
        private IOrderRepository orders = null;

        /// <summary>
        /// Public property to handle the access of our Orders Repository
        /// </summary>
        public IOrderRepository Orders
        {
            get
            {
                if (orders == null)
                {
                    orders = new OrderRepository();
                }

                return orders;
            }
        }
    }
}
