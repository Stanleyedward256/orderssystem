﻿using System.Collections.Generic;
using Domain;

namespace Repository
{
    /// <summary>
    /// Represents an orders repository
    /// </summary>
    public interface IOrderRepository
    {
        /// <summary>
        /// All the orders in the system
        /// </summary>
        /// <returns>A collection of all orders in the system</returns>
        List<Order> GetOrders();

        /// <summary>
        /// Obtain Order items for a specified order.
        /// </summary>
        /// <param name="orderRef">The reference of the order to load</param>
        /// <returns>A collection of order items for the specified order</returns>
        List<OrderItem> GetOrderItems(string orderRef);
    }
}
