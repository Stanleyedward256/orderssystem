﻿/**
    An Object to contain all our logic in one place.
*/
var $OrderSystem = function () {

    let orderSystem = {};

    const ordersHolderIdentifier = "#orders-holder";
    const accordionContentIdentifier = ".uk-accordion-content";
    /**
        The orders that have been loaded in a call from the API.
    */
    orderSystem.currentOrders = [];

    /**
         A jQuery DOM object representing our error message if we cannot load the orders list at all.
    */
    orderSystem.listError = $('<div class="uk-alert-danger" uk-alert><p><span uk-icon="warning"></span>&nbsp;Unable to load order list, perhaps the API has stopped?</p></div>');

    /**
        A jQuery DOM object representing our warning message if an order doesn't have any items on it.
     */
    orderSystem.orderHasNoItemsTemplate = $('<div class="uk-alert-warning" uk-alert><p><span uk-icon="warning"></span>&nbsp;This order has no items on it</p></div>');

    /**
        {string} template representing the accordion element that will be duplicated for each item.
        It is a string because we will replace the {tags} with data from our object
    */
    orderSystem.listItemTemplate = '<li><a class="uk-accordion-title" href="#">{ref} - {cost}</a><div class="uk-accordion-content"></div></li>';

    /**
        {string} template representing the start of our item table element, it contains the heading columns and the start of the tbody.
    */
    orderSystem.itemTableStartTemplate = '<table class="uk-table uk-table-striped"><thead><tr><th>Product Code</th><th>Description</th><th>Line Cost</th><th>Quantity</th><th>Cost</th></tr></thead><tbody>';

    /**
        {string} template representing the end of our table.
    */
    orderSystem.itemTableEndTemplate = "</tbody></table>";


    /**
        Makes a call to the server for our current orders list. 
    */
    orderSystem.loadOrders = function () {
        $.ajax({
            url: "https://localhost:44347/api/Order",
            success: (data) => {
                $OrderSystem.currentOrders = data;
                $OrderSystem.handleOrderListSuccess();

            },
            error: () => {
                $OrderSystem.handleOrderListFailure();
            }
        });
    };

    /**
        Handles the displaying of an error message to the user if the list fails to load.
    */
    orderSystem.handleOrderListFailure = function () {
        let ordersHolder = $(ordersHolderIdentifier);
        ordersHolder.empty();
        ordersHolder.append($OrderSystem.listError.clone());
    };

    /**
        Handles the population of our the accordion and item tables if we can load some orders.
    */
    orderSystem.handleOrderListSuccess = function () {
        $OrderSystem.currentOrders.forEach(order => {
            let details = $OrderSystem.listItemTemplate.replace("{ref}", order.ref).replace("{cost}", order.formattedOrderTotal);
            let detailsElement = $(details);
            if (order.items.length == 0) {
                let accordionContentElement = detailsElement.find(accordionContentIdentifier);
                accordionContentElement.empty();
                accordionContentElement.append($OrderSystem.orderHasNoItemsTemplate.clone());
            }
            else {
                let itemTableHtml = $OrderSystem.itemTableStartTemplate;
                order.items.forEach(item => {
                    itemTableHtml += "<tr><td>" + item.code + "</td><td>" + item.description + "</td><td>" + item.formattedCost + "</td><td>" + item.quantity + "</td><td>" + item.formattedTotalCost + "</td></tr>";
                });
                itemTableHtml += $OrderSystem.itemTableEndTemplate;

                detailsElement.find(accordionContentIdentifier).html(itemTableHtml);
            }
            $(ordersHolderIdentifier).append(detailsElement.clone());
        });
        
    }

    return orderSystem;
}();