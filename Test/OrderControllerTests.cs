using NUnit.Framework;
using Moq;
using API.Controllers;
using Domain;
using Repository;
using System.Collections.Generic;
using Newtonsoft;

namespace Test
{
    [TestFixture]
    public class OrderControllerTests
    {
        private readonly OrderController SubjectUnderTest;
        private readonly Mock<IDataRepository> DataRepositoryMock;
        private readonly Mock<IOrderRepository> OrderRepositoryMock;

        public OrderControllerTests()
        {
            DataRepositoryMock = new Mock<IDataRepository>() { DefaultValue = DefaultValue.Mock };
            // We want to be able to mockup the orderRepository's methods too.
            OrderRepositoryMock = Mock.Get(DataRepositoryMock.Object.Orders);
            SubjectUnderTest = new OrderController(DataRepositoryMock.Object);
        }

        [SetUp]
        public void Setup()
        {
            
        }

        /// <summary>
        /// Tests to see if all the orders are loaded correctly through the controller.
        /// </summary>
        [Test]
        public void GetOrders_ShouldReturnAllOrders()
        {
            // Arrange
            var _orders = new List<Order>
            {
                new Order
                {
                    Ref = "ABC123",
                    Items = new List<OrderItem>
                    {
                        new OrderItem {Code = "HAT", Description = "Wool Hat with bobble on top", Cost = (decimal) 5.00, Quantity = 1},
                        new OrderItem {Code = "JUMPER", Description = "Knitted style jumper with large buttons", Cost = (decimal) 12.65, Quantity = 2},
                        new OrderItem {Code = "TROUSERS", Description = "Long Slim Fit Denim Jeans", Cost = (decimal) 35.25, Quantity = 3}
                    }
                },
                new Order
                {
                    Ref = "DEF456",
                    Items = new List<OrderItem>
                    {
                        new OrderItem {Code = "HAT", Description = "Wool Hat with bobble on top", Cost = (decimal) 5.00, Quantity = 1},
                        new OrderItem {Code = "JUMPER", Description = "Knitted style jumper with large buttons", Cost = (decimal) 12.65, Quantity = 2}
                    }
                },
                new Order
                {
                    Ref = "GHI456"
                },
            };

            OrderRepositoryMock.Setup(o => o.GetOrders()).Returns(_orders);

            // Act
            var result = SubjectUnderTest.Get();

            // Assert
            Assert.AreEqual(Newtonsoft.Json.JsonConvert.SerializeObject(_orders), Newtonsoft.Json.JsonConvert.SerializeObject(result));
        }
    }
}